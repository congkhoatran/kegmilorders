package com.example.KegmilOrder.controller;
import com.example.KegmilOrder.domain.Order;
import com.example.KegmilOrder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderController {

    @Autowired
    OrderService ORDER_SERVICE;

    @GetMapping(path = "/exportToPdf", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> exportToPdf() {
        List<Order> orders = ORDER_SERVICE.findAll();
        return orders;
    }
}
