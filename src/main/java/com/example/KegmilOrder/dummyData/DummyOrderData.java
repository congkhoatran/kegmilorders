package com.example.KegmilOrder.dummyData;

import com.example.KegmilOrder.domain.Order;

import java.util.*;

public class DummyOrderData {
    public static List<Order> getDummyData() {
        List<Order> orders = new ArrayList<Order>();
        orders.add(new Order(1, "order 1","son.tranhong@gmail.com", 40000));
        orders.add(new Order(2, "order 2","tien@gmail.com", 5000));
        orders.add(new Order(3, "order 3","khoa@gmail.com", 7000));
        orders.add(new Order(4, "order 4","tin@gmail.com", 11000));

        return orders;
    }
}
