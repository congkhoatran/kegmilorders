package com.example.KegmilOrder.domain;

import com.example.KegmilOrder.dummyData.DummyOrderData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDAO {

    public OrderDAO() {}

    public List<Order> findAll() {
        return DummyOrderData.getDummyData();
    }
}
