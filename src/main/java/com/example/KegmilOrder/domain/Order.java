package com.example.KegmilOrder.domain;

public class Order {
    public long id;
    public String orderName;
    public String email;
    public double price;

    public Order(long id, String orderName, String email, double price) {
        this.id = id;
        this.orderName = orderName;
        this.email = email;
        this.price = price;
    }
}
