package com.example.KegmilOrder.service;

import com.example.KegmilOrder.domain.Order;
import com.example.KegmilOrder.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Configurable
public class OrderService {

    @Autowired
    OrderRepository ORDER_REPOSITORY;

    public List<Order> findAll() {
        return ORDER_REPOSITORY.findAll();
    }
}
