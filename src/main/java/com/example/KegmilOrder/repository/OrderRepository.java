package com.example.KegmilOrder.repository;

import com.example.KegmilOrder.domain.Order;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository {
    List<Order> findAll();
}
