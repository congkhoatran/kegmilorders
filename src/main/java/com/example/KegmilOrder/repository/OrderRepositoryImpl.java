package com.example.KegmilOrder.repository;

import com.example.KegmilOrder.domain.Order;
import com.example.KegmilOrder.domain.OrderDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    OrderDAO ORDER_DAO;

    @Override
    public List<Order> findAll() {
        return ORDER_DAO.findAll();
    }
}
