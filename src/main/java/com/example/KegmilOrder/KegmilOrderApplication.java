package com.example.KegmilOrder;

import com.example.KegmilOrder.controller.OrderController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class KegmilOrderApplication {
	public static void main(String[] args) {
		SpringApplication.run(KegmilOrderApplication.class, args);
	}
}
