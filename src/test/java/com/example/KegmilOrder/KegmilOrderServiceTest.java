package com.example.KegmilOrder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.example.KegmilOrder.domain.Order;
import com.example.KegmilOrder.repository.OrderRepository;
import com.example.KegmilOrder.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class KegmilOrderServiceTest {
    @Autowired
    private OrderService orderService;

    @MockBean
    private OrderRepository orderRepository;

    @Before
    public void initUserRepository() {
        when(this.orderRepository.findAll()).thenReturn(
                Arrays.asList(	new Order(1, "Order 1","son.tranhong@gmail.com", 1000),
                        new Order(2, "Order 2","tieng@gmail.com", 50000),
                        new Order(3, "Order 3","khoa@gmail.com", 7000)
                ));
    }

    @Test
    public void findAllTest() {
        List<Order> orders = orderService.findAll();

        for(Order order : orders)
            System.out.printf("Order name=%s, email=%s \n"
                    , order.orderName
                    , order.email
            );

        assertThat(orders.get(1).orderName)
                .isEqualTo("Order 1");

    }
}
